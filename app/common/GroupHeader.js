import React from 'react';
import styled from 'styled-components';


const BannerSec = styled.div`
position: relative;
    background: #eee;
    `;

const BannerImg = styled.img`
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: auto;
    height: 327px;
    border-radius: 10px 10px 0px 0px;
    background: #fff;
    @media only screen and (max-width:480px) {
    width:100%;

    }

`;



const GroupDetailFoot = styled.div`
    padding: 20px 25px 25px;
    background: #FFF;
    max-width: 470px;
    border-radius: 10px;
    position: absolute;
    bottom: -52px;
    margin-left: 153px;
    /* max-width: 94%; */
    /* left: 11px; */
    /* padding: 15px; */
    @media only screen and (max-width:480px) {

     padding: 15px 15px 15px;
    max-width: 339px;
    bottom: -52px;
    margin-left: 20px;
    margin-top: 20px;
    /* width: 78%; */
    height: 50%;
    line-height: -10;
    }
    
`;

const GroupName = styled.label`

text-align: left;
    font: 40px/42px Lato;
    letter-spacing: 0;
    color: #BB4D9A;
`;
const GroupDetails = styled.div`

margin-top: 30px;
    display: inline-block;
display:flex;
	flex-flow:row;
justify-content:space-between;
@media only screen and (max-width:480px) {
    margin-top: 5px;

}

`;

const Stamp = styled.label`
  font: bold 10px/18px Lato;
  letter-spacing: 0;
  color: #212529;
  text-transform: uppercase;
  display: block;
  font-family: "Lato", sans-serif;

  `;
const StampMri = styled.label`
  font: bold 10px/18px Lato;
  letter-spacing: 0;
  color: #212529;
  text-transform: uppercase;
  display: block;
  margin-right: 25px !important;
  font-family: "Lato", sans-serif;

  `;


const Detail = styled.span`
  text-align: left;
  font: bold 14px/14px Lato;
  letter-spacing: 0;
  color: #BB4D9A;
  display: block;
  margin-top: 5px;
  `;


const GroupHeader = (props) => {
    return (
        <BannerSec>
            <BannerImg
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR9nZJjTnFEm_CFh33YVtM_r8stemBCIRzLfQ&usqp=CAU"
            //  src={props.group.image} 
             alt="group banner" name="groupbannerimage"></BannerImg>
            <GroupDetailFoot>
                <div className="grpDesc">
                    <GroupName>{props.group.name}</GroupName>
                </div>
                <GroupDetails>
                    <div className="">
                        <Stamp>GROUPTYPE</Stamp>
                        <Detail>{props.group.groupType}</Detail>
                    </div>



                    {/* <div className="">
            <label className="stamp">Members</label>
            <span className="detail">{this.state.group.numMembers}</span>
        </div>
        <div className="">
            <label className="stamp grpdistance">DISTANCE</label>
            <span className="detail">0</span>
        </div> */}

                    {/*
        <div className="">
            <label className="stamp mri">ARTICLES</label>
            <span className="detail">{this.state.totalArticles}</span>
        </div>

    </div>
    <div className="grpDetails flexRow evenSpace">

        <div className="">
            <label className="stamp">LOCATION</label>
            <span className="detail">{this.state.group.address}</span>
        </div>
        <div className="">
            <label className="stamp"></label>
            <span className="detail"></span>
        </div>
    </div>
</div>
</div>

    */}

                    <div className="">
                        <StampMri>ARTICLES</StampMri>

                        <Detail>{props.articlecount}</Detail>
                    </div>

                </GroupDetails>
                <GroupDetails>

                    <div className="">
                        <Stamp>Location</Stamp>
                        <Detail>{props.group.address}</Detail>

                    </div>
                </GroupDetails>


            </GroupDetailFoot>

        </BannerSec>
    )
}

export default GroupHeader;