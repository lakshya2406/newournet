import React, { Component } from "react";
import Header from "./Header";
import Footer from "./Footer";
import moment from "moment";
import {
  Row,
  Col,
  Container,
  Modal,
  Button,
  Table,
  Dropdown,
} from "react-bootstrap";
import "../../style/component/AboutGroup.css";
import "../../style/component/HomeArticles.css";
import * as PopActions from "../actions/groupActions";
import * as myarticleAction from "../actions/myarticleAction";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";
import { getOrientation } from "get-orientation";
import Meta from "../common/Meta";
import GroupArticle from "./GroupArticle";
import ArticleCards from "../common/articleCards";
import GroupHeader from "../common/GroupHeader";
import "./aboutGroup.css";

const FullArticle = styled.div`
  padding: 130px 143px 50px 143px;
`;

const PopularTitle = styled.label`
  color: #024b99;
  font-size: 20px;
`;

const PopularArticles = styled.div`
  display: grid;
  grid-gap: 15px;
  grid-template-columns: repeat(auto-fill, minmax(320px, 1fr));
  grid-auto-rows: 0px;
`;

const Article = styled.div`
  border-radius: 10px;
  margin: 30px;
  float: left;
`;
const ArticleImg = styled.img`
  width: 100%;
  height: 170px;
  background: #fff;
  border-radius: 10px 10px 0px 0px;
`;

const ArticleFoot = styled.div`
  padding: 15px;
  background: #fff;
  border-radius: 0px 0px 10px 10px;
`;

const ArticleLink = styled.div`
  position: relative;
  display: block;
  background: #fff;
`;

const ArticleTitle = styled.p`
  font: Bold 14px/16px Lato;
`;
const ArticleWriter = styled.label`
  font: Bold 14px/14px Lato;
  letter-spacing: 0;
  color: #000000;
  margin-top: 5px;
`;
const ArticleActions = styled.label`
  margin-top: 15px;
  align-items: center;

  display: flex;
  flex-flow: row;
  justify-content: space-between;
`;
const TimeStamp = styled.label`
  font: Bold 13px/14px Lato;
  letter-spacing: 0;
  color: #024b99;
`;
const LeaveGroupButton = styled.button`
  width: auto;
  height: 60px;
  margin: 0px 25px 50px 25px;
  padding: 0 50px;

  position: relative;
  overflow: hidden;
  background: #4fbfae;
  border: 1px solid #4fbfae;
  border-radius: 10px;
  text-align: center;
  font: Bold 16px/19px Lato;
  letter-spacing: 0;
  text-transform: uppercase;
  cursor: pointer;
  color: #fff;
  transform: translateX(0);
  background: transparent !important;
  border: 1px solid #fb531f !important;
  color: #fb531f !important;
`;
const InviteFriendButton = styled.button`
  width: auto;
  height: 60px;
  margin: 0px 25px 50px 25px;
  padding: 0 50px;
  position: relative;
  overflow: hidden;
  background: #4fbfae;
  border: 1px solid #4fbfae;
  border-radius: 10px;
  text-align: center;
  font: Bold 16px/19px Lato;
  letter-spacing: 0;
  text-transform: uppercase;
  cursor: pointer;
  color: #fff;
  transform: translateX(0);
`;
const UpBtn = styled.button`
  background: #f9a61b;
  height: 40px;
  width: 40px;
  line-height: 40px;
  border-radius: 50%;
  border: none;
  color: #fff;
  cursor: pointer;
  font-size: 1.5rem;
  margin-top: 75px;
  outline: none;
`;
class AboutGroups extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      group: [],
      groupId: this.props.groupId,
      articles: [],
      pageIndex: 0,
      pageSize: 12,
      filter: "",
      totalArticles: "",
      currentUserMember: false,
      dimg: "../../img/design.png",
      show: false,
      sorting: "PUBLISHED",
      articlesUser: [],
      group_articles: [],
      groupsof: [],
      got: [],
      articleId: [],
      id: [],
      checked: [],
      lgShow: false,
      members: [],
      searchResults: [],
    };
  }

  componentDidMount() {
    this.props.popActions
      .getGroupArticles(
        this.props.groupId,
        this.state.pageIndex,
        this.state.pageSize,
        this.state.filter
      )
      .then((resp) => {
        this.setState({
          totalArticles: resp.totalArticles,
          articles: resp.articles,
        });
        this.resize();
      });

    this.fetchGroupMembers();

    this.fetchArticles(
      this.state.pageIndex,
      this.state.pageSize,
      this.state.sorting
    );
    this.fetchGroupArticles(
      this.props.groupId,
      this.state.pageSize,
      this.state.pageIndex,
      this.state.filter
    );
    if (this.props.currentgroup == null) {
      this.fetchGroups(this.props.groupId);
    } else {
      console.log("group name", this.props.currentGroup);
      this.setState({
        group: this.props.currentgroup,
        currentUserMember: this.props.currentgroup.currentUserMember,
      });
    }
    window.scrollTo(0, 0);
  }

  // componentDidUpdate = (prevProps , prevState) =>{

  //     if(this.state.articles.length!=prevState.articles.length){
  //         console.log()
  //         this.setState({
  //             id:this.state.articles
  //         })
  //     }
  // }

  fetchArticles = (pageIndex, pageSize, sorting) => {
    this.props.myarticleAction
      .getMyArticles(pageIndex, pageSize, sorting)
      .then((resp) => {
        console.log("Articles user", resp);
        let arr = [];
        for (let i = 0; i < resp.articles.length; i++) {
          arr[i] = resp.articles[i].id;
        }

        let checked = [];
        for (let i = 0; i < arr.length; i++) {
          checked[i] = 0;
        }

        console.log(arr);

        //console.log("users all articles ids =", resp.articles[0].id  );
        this.setState({
          articlesUser: resp.articles,
          articleId: arr,
          fetching: false,
          //checked:checked
        });
      });
  };

  fetchGroupArticles = (groupId, pageSize, pageIndex, filter) => {
    this.props.popActions
      .getGroupArticles(groupId, pageIndex, pageSize, filter)
      .then((resp) => {
        console.log("new one of mine", resp);

        let arr = [];
        for (let i = 0; i < resp.articles.length; i++) {
          arr[i] = resp.articles[i].id;
        }

        console.log(arr);
        //console.log("id os group articles ", resp.articles[0].id);

        this.setState({
          id: arr,

          group_articles: resp.group_articles,
        });
        this.resize();
      });
  };
  handleClose = () => {
    this.setState({
      show: false,
    });
  };

  findMember = (e) => {
    if (e.target.value.length >= 4) {
      console.log("string length", e.target.value.length);
      let data = e.target.value;
      // let data = {
      // groupId: this.state.groupId,
      // userName:e.target.value,
      // sortColumn:"firstName",
      // sortType:"ASC"
      // }
      this.props.popActions.findMemberGlobal(data).then((resp) => {
        console.log("findMemberGlobal data: ", resp);
        this.setState({
          searchResults: resp,
        });
      });
    }
  };

  removeMember = (Id) => {
    let data = {
      groupId: this.state.groupId,
      userId: Id,
    };
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.props.popActions.removeMember(data).then((resp) => {
          swal({
            title: "Member removed!",
            // text: 'The Article was Published Successfully',
            icon: "success",
            timer: 810,
          });
          this.fetchGroupMembers();
        });
      }
    });
  };

  makeAdmin = (Id) => {
    let data = {
      groupId: this.state.groupId,
      userId: Id,
    };
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.props.popActions.makeAdmin(data).then((resp) => {
          console.log("Make Admin:", resp)
          swal({
            title: "Member is now Admin!",
            // text: 'The Article was Published Successfully',
            icon: "success",
            timer: 810,
          });
          this.fetchGroupMembers();
        });
      }
    });
  };

  addMembertoGroup = (Id) => {
    let data = {
      groupId: this.state.groupId,
      userId: Id,
    };
    this.props.popActions.addMember(data).then((resp) => {
      console.log("member added: ", resp);
      console.log("currentUserMember: ", resp.currentUserMember);
      swal({
        title: "Member added successfully",
        // text: "Article has been published successfully",
        icon: "success",
        timer: 900,
      });
      this.fetchGroupMembers();
      this.setState({
        searchResults: [],
      });
    });
  };

  fetchGroupMembers = () => {
    this.props.popActions.getGroupMembers(this.props.groupId).then((resp) => {
      // console.log("call Group members resp:", resp);
      this.setState({
        members: resp,
      });
    });
  };

  handleShow = () => {
    this.setState({
      show: true,
    });
  };
  isPresent = (index) => {
    if (this.state.id.indexOf(this.state.articleId[index]) != -1) {
      return true;
    }
    return false;
  };
  isAbsent = (e, index) => {
    let articleId = this.state.articleId[index];
    let arr = [];
    arr = this.state.id;
    let idIndex = arr.indexOf(articleId);
    if (idIndex != -1) {
      arr.splice(idIndex, 1);
    }
    if (idIndex == -1) {
      arr.push(articleId);
    }

    this.setState({
      id: arr,
      checked: false,
    });
  };

  // resize() {
  //     function resizeGridItem(item) {
  //         let grid = document.getElementsByClassName("popularArticles")[0];
  //         let rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
  //         let rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
  //         let rowSpan = Math.ceil((item.querySelector('.content').getBoundingClientRect().height + rowGap) / (rowHeight + rowGap));
  //         item.style.gridRowEnd = "span " + rowSpan;
  //     }

  //     function resizeAllGridItems() {
  //         let allItems = document.getElementsByClassName("article");
  //         for (let x = 0; x < allItems.length; x++) {
  //             resizeGridItem(allItems[x]);
  //         }
  //     }
  //     window.onload = resizeAllGridItems();
  //     window.addEventListener("resize", resizeAllGridItems);
  // }

  resize() {
    function resizeGridItem(item) {
      let grid = document.getElementsByClassName("first")[0];
      let rowHeight = parseInt(
        window.getComputedStyle(grid).getPropertyValue("grid-auto-rows")
      );
      let rowGap = parseInt(
        window.getComputedStyle(grid).getPropertyValue("grid-row-gap")
      );
      let rowSpan = Math.ceil(
        (item.querySelector(".card").getBoundingClientRect().height + rowGap) /
          (rowHeight + rowGap)
      );
      item.style.gridRowEnd = "span " + rowSpan;
    }

    function resizeAllGridItems() {
      let allItems = document.getElementsByClassName("articleCombine");
      for (let x = 0; x < allItems.length; x++) {
        resizeGridItem(allItems[x]);
      }
    }
    window.onload = resizeAllGridItems();
    window.addEventListener("resize", resizeAllGridItems);
  }

  fetchGroups = (groupId) => {
    this.props.popActions.getGroupInfo(groupId).then((group) => {
      this.setState({
        group: group.group,
        currentUserMember: group.currentUserMember,
      });
    });
  };

  groupAction = (flag) => {
    let data = {};
    data.groupId = this.state.groupId;
    if (flag == 0) {
      this.props.popActions.LeaveGroup(data).then((resp) => {
        this.setState({
          currentUserMember: resp.currentUserMember,
        });
        console.log("Leave Group response", resp);
      });
    }
    if (flag == 1) {
      this.props.popActions.JoinGroup(data).then((resp) => {
        this.setState({
          currentUserMember: resp.currentUserMember,
        });
        console.log("Join Group response", resp);
      });
    }
  };

  scrollup() {
    document.body.scrollTo({ top: 0, behavior: "smooth" });
    document.documentElement.scrollTo({ top: 0, behavior: "smooth" });
  }

  onArticleLink = () => {
    const data = {
      articleId: this.state.id,
      groupId: this.props.groupId,
    };

    this.props.popActions.updateGroupArticles(data).then((res) => {
      this.props.popActions
        .getGroupArticles(
          this.props.groupId,
          this.state.pageIndex,
          this.state.pageSize,
          this.state.filter
        )
        .then((resp) => {
          this.setState({
            totalArticles: resp.totalArticles,
            articles: resp.articles,
          });
          this.resize();
        });

      this.setState({
        show: false,
      });
    });
  };

  render() {
    // console.log("group naem", this.state.group);
    const { group } = this.state;
    return (
      <React.Fragment>
        <Meta title={this.state.group.name} />
        {/* <Header currentUrl={this.props.currentUrl} /> */}
        {/*
               <div className="bannerSec">
                    <img src={group.image} alt="group banner" name="groupbannerimage" className="bannerImg" />
                    <div className="grpDetailFoot describergrp">
                        <div className="grpDesc">
                            <label className="grpName">{this.state.group.name}</label>
                        </div>
                        <div className="grpDetails flexRow evenSpace">
                            <div className="">
                                <label className="stamp">GROUPTYPE</label>
                                <span className="detail">{this.state.group.groupType}</span>
                            </div>
                */}
        <Container fluid>
          <div className="row">
            <div className="rightcolumn">
              <GroupHeader
                group={this.state.group}
                articlecount={this.state.totalArticles}
              />

              <div className="fullArticlegrp">
                {/* Modal start */}
                <If
                  condition={
                    (this.state.group.currentUserOwner ||
                      this.state.group.currentUserAdmin) &&
                    (this.state.group.groupType == "PRIVATE" ||
                      this.state.group.groupType == "INVITED_ONLY")
                  }
                >
                  <div>
                    <button
                      className="inviteFriendBtn btn-fill-blue"
                      onClick={() => this.setState({ lgShow: true })}
                    >
                      Manage Group
                    </button>
                  </div>
                  <Modal
                    style={{ marginTop: "6.5em", zIndex: 9999 }}
                    size="lg"
                    show={this.state.lgShow}
                    onHide={() => this.setState({ lgShow: false })}
                    aria-labelledby="example-modal-sizes-title-lg"
                  >
                    <Modal.Header closeButton>
                      <Modal.Title id="example-modal-sizes-title-lg">
                        Manage Group
                      </Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{ overflow: "auto", height: "30em" }}>
                      {/* Table start */}
                      <span>Enter member name to invite</span>
                      <input
                        className=""
                        id="inviteMember"
                        onChange={(e) => this.findMember(e)}
                      />
                      <If condition={this.state.searchResults.length}>
                        <Table striped bordered hover size="sm">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Username</th>
                              <th>email</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.members &&
                              this.state.searchResults.map((member, index) => {
                                return (
                                  <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{member.firstName}</td>
                                    <td>{member.lastName}</td>
                                    <td>{member.username}</td>
                                    <td>{member.email}</td>
                                    <td>
                                      <Button
                                        variant="primary"
                                        onClick={() =>
                                          this.addMembertoGroup(member.id)
                                        }
                                      >
                                        add member
                                      </Button>
                                    </td>
                                  </tr>
                                );
                              })}
                          </tbody>
                        </Table>
                      </If>
                      <Table striped bordered hover size="sm">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.members &&
                            this.state.members.map((member, index) => {
                              return (
                                <tr key={index}>
                                  <td>{index + 1}</td>
                                  <td>{member.firstName}</td>
                                  <td>{member.lastName}</td>
                                  <td>{member.username}</td>
                                  <td>
                                    <Dropdown>
                                      <Dropdown.Toggle
                                        variant="danger"
                                        id="dropdown-basic"
                                        className="dropbtnB"
                                      >
                                        Action
                                      </Dropdown.Toggle>
                                      <Dropdown.Menu className="dropdown-content">
                                        <If condition={!member.admin}>
                                        <Dropdown.Item
                                          onClick={() =>
                                            this.makeAdmin(member.id)
                                          }
                                        >
                                          {" "}
                                          <p>Make Admin</p>{" "}
                                        </Dropdown.Item>
                                        </If>
                                        <Dropdown.Item
                                          onClick={() =>
                                            this.removeMember(member.id)
                                          }
                                        >
                                          {" "}
                                          <span className="colorfontclass">
                                            remove
                                          </span>{" "}
                                        </Dropdown.Item>
                                      </Dropdown.Menu>
                                    </Dropdown>
                                  </td>
                                </tr>
                              );
                            })}
                        </tbody>
                      </Table>

                      {/* Table end */}
                    </Modal.Body>
                  </Modal>
                  {/* Modal end */}
                </If>

                <div className="sC-buton">
                  <PopularTitle>
                    <b> COMMUNITY ARTICLES </b>
                  </PopularTitle>
                  <Link
                    to={{
                      pathname: "/newarticle",
                      group: this.state.group,
                    }}
                  >
                    <button
                      type="button"
                      className="inviteFriendBtn btn-fill-blue"
                    >
                      CREATE ARTICLE{" "}
                    </button>
                  </Link>

                  <label className="LinkArticle">
                    {this.state.group.groupType == "PUBLIC" && (
                      <button
                        type="button"
                        className="inviteFriendBtn btn-fill-blue"
                        onClick={() => this.setState({ show: true })}
                      >
                        LINK ARTICLES
                      </button>
                    )}
                  </label>
                </div>
                <ArticleCards
                  group={this.state.group}
                  articles={this.state.articles}
                />
                {/* <div className="popularArticles">

                         {this.state.articles.map((article) => {
                            return (
                                <div className="article" key={article.id}>
                                    <div className="content">
                                        <img src={article.image ? article.image : this.state.dimg} alt="article image" name="articleimg" className="articleImg" />
                                        <div className="articleFoot">
                                            <div className="articleDesc">
                                                <Link to={`/article/${article.slug}`} className="article_link">
                                                    <p className="articleTitle">{article.title}</p>
                                                    <p className="articleIntro">{article.introduction}</p>
                                                </Link>
                                                <label className="articleWriter">Michael Brodie</label>
                                            </div>
                                            <div className="articleActions flexRow evenSpace">
                                                <label className="timeStamp">{moment(article.createdDate).format("MMM D, YYYY")}</label>
                                                <div>
                                                    <span className="action actionCmnt"><i className="fa fa-comment-o" aria-hidden="true"></i></span>
                                                    <span className="action actionBookmrk"><i className="fa fa-bookmark" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            );
                        })
                        }
                    </div>*/}
              </div>

              <div className="haCenter">
                <Choose>
                  <When condition={!this.props.isLoggedIn}>
                    <Link to="/login">
                      <button
                        type="button"
                        className="leaveGrpBtn btn-fill-blue btn-org-border"
                      >
                        JOIN COMMUNITY
                      </button>
                    </Link>
                  </When>
                </Choose>
                {/*  <div className="LinkArticle">
                        {this.state.group.groupType == "PUBLIC" &&
                            <button type="button" className="inviteFriendBtn btn-fill-blue" onClick={() => this.setState({ show: true })}>LINK ARTICLES</button>
                        }
                    </div> */}

                <Choose>
                  <When condition={this.props.isLoggedIn}>
                    <button
                      type="button"
                      className={
                        this.state.currentUserMember
                          ? "leaveGrpBtn btn-fill-blue btn-org-border"
                          : "inviteFriendBtn  btn-fill-blue btn-org-border"
                      }
                      onClick={(e) =>
                        this.state.currentUserMember
                          ? this.groupAction(0)
                          : this.groupAction(1)
                      }
                    >
                      {this.state.currentUserMember
                        ? "LEAVE GROUP"
                        : "JOIN GROUP"}
                    </button>
                  </When>
                </Choose>
                <button type="button" className="inviteFriendBtn btn-fill-blue">
                  INVITE FRIENDS
                </button>

                {/*}  <Link to={{
                        pathname:"/newarticle",
                        group:this.state.group
                    }} >
                    <button type="button" className="inviteFriendBtn btn-fill-blue">CREATE ARTICLE</button>
                  
                </Link>*/}
              </div>
              {/*  {this.e ? joinGroup(this.props.groupId) : */}
              {/* <div className="rightfixedbox">
                        <button type="button" className="commentBtn">
                            <i className="fa fa-comment"></i>
                        </button>
                    </div> */}

              <div>
                <Modal
                  show={this.state.show}
                  onHide={() => this.handleClose()}
                  size="lg"
                  aria-labelledby="contained-modal-title-vcenter"
                  centered
                >
                  <Modal.Header closeButton>
                    <Modal.Title>
                      {" "}
                      Link Your Article With {this.state.group.name}
                    </Modal.Title>
                  </Modal.Header>

                  <Modal.Body
                    style={{
                      maxHeight: "calc(100vh - 210px)",
                      overflowY: "auto",
                    }}
                  >
                    <div className="grid-container">
                      {this.state.articlesUser.map((article, index) => (
                        <div
                          key={index}
                          className={
                            this.isPresent(index) == true
                              ? "grid-item-T"
                              : "grid-item"
                          }
                          onClick={(e) => this.isAbsent(e, index)}
                        >
                          <div className="articleCombine card-content">
                            <div className="cardModal">
                              <div className="imgWrapperCentre">
                                <img
                                  src={article.image}
                                  className="card-img-top-Modal"
                                  alt="..."
                                />
                              </div>
                              <div className="card-body secondModal">
                                <h5 className="card-title-Modal">
                                  {article.title}
                                </h5>
                                <p className="articleIntro">
                                  {article.introduction
                                    .split("")
                                    .splice(0, 20)
                                    .join("")}
                                </p>
                              </div>
                            </div>
                            <label className="shift-right">
                              <input
                                type="checkbox"
                                className="checkStyle"
                                checked={this.isPresent(index)}
                              />
                              <span className="checkmark"></span>
                            </label>
                          </div>
                        </div>
                      ))}
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button
                      variant="secondary"
                      onClick={() => this.handleClose()}
                    >
                      Close
                    </Button>
                    <Button
                      variant="primary"
                      onClick={() => this.onArticleLink()}
                    >
                      Save Changes
                    </Button>
                  </Modal.Footer>
                </Modal>
              </div>
            </div>
          </div>
        </Container>
        <div className="haCenter">
          <button
            type="button"
            className="upBtn"
            onClick={(e) => this.scrollup(e)}
          >
            <i className="fa fa-angle-up" aria-hidden=""></i>
          </button>
        </div>

        <Footer />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state, ownProps) {
  let currentUrl = ownProps.location ? ownProps.location.pathname : "";
  let groupId = ownProps.match.params.id;
  let currentgroup = state.Groups.groups.find((x) => x.id == groupId);
  if (currentgroup == 0) {
    currentgroup = state.Groups.group.find((x) => x.id == groupId);
  }
  let isLoggedIn = state.Authentication.loggedIn;

  return {
    currentUrl,
    currentgroup,
    groupId,
    isLoggedIn,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    popActions: bindActionCreators(PopActions, dispatch),
    myarticleAction: bindActionCreators(myarticleAction, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AboutGroups);
